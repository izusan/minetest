creative_mode = false
auth_backend = sqlite3
player_backend = sqlite3
gameid = minetest
enable_damage = true
backend = sqlite3
load_mod_mobs = true
load_mod_mobs_animal = true
load_mod_mobs_monster = true
load_mod_sfinv_buttons = true
load_mod_unified_inventory = true
load_mod_craftguide = true
load_mod_basic_materials = true
load_mod_unifieddyes = true
load_mod_homedecor_common = true
load_mod_homedecor_electronics = true
load_mod_homedecor_climate_control = true
load_mod_homedecor_books = true
load_mod_homedecor_bathroom = true
load_mod_homedecor_bedroom = true
load_mod_homedecor_electrical = true
load_mod_homedecor_3d_extras = true
load_mod_computer = true
load_mod_homedecor_pictures_and_paintings = true
load_mod_homedecor_foyer = true
load_mod_homedecor_furniture = true
load_mod_homedecor_doors_and_gates = true
load_mod_homedecor_misc = true
load_mod_homedecor_tables = true
load_mod_homedecor_exterior = true
load_mod_homedecor_fences = true
load_mod_homedecor_gastronomy = true
load_mod_homedecor_furniture_medieval = true
load_mod_homedecor_kitchen = true
load_mod_plasmascreen = true
load_mod_homedecor_windows_and_treatments = true
load_mod_building_blocks = true
load_mod_homedecor_laundry = true
load_mod_homedecor_lighting = true
load_mod_inbox = true
load_mod_lavalamp = true
load_mod_homedecor_cobweb = true
load_mod_homedecor_office = true
load_mod_homedecor_trash_cans = true
load_mod_homedecor_roofing = true
load_mod_homedecor_seating = true
load_mod_homedecor_clocks = true
load_mod_fake_fire = true
load_mod_homedecor_wardrobe = true
load_mod_itemframes = true
