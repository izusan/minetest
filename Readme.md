# Teampail's Minetest server

Add-ons et paramétrage pour les serveurs Minetest de la Teampail ! Les mods et textures référencés dans cette doc s'appliquent à la branch master (stable). Les autres branches peuvent diverger.

## 3 serveurs

adresse : minetest.teampail.ovh:3000'x'

| /           | Stable | RC                                                  | Test                                      |
|-------------|--------|-----------------------------------------------------|-------------------------------------------|
| Port        | 30000  | 30001                                               | 30002                                     |
| Utilisation | Jeu    | Réplique de 'stable' avec des mods en cours de test | creatif et instable, pour tester des mods |
| Branche     | master | rc                                                  | test                                      |

## Mods

*   [basic_materials](https://forum.minetest.net/viewtopic.php?f=11&t=21000)
*   [craftguide](https://forum.minetest.net/viewtopic.php?f=11&t=14088)
*   [mobs](https://forum.minetest.net/viewtopic.php?f=11&t=9917)
*   mobs_animals
*   mobs_monster
*   [sfinv_buttons](https://forum.minetest.net/viewtopic.php?t=16079)
*   [unified_inventory](https://forum.minetest.net/viewtopic.php?t=12767)
*   [unifieddyes](https://forum.minetest.net/viewtopic.php?f=11&t=2178)
*   [Homedecor modpack](https://forum.minetest.net/viewtopic.php?t=2041)

## Textures

[PixelPerfection](https://forum.minetest.net/viewtopic.php?t=14289)

## Configuration

Pas de modification particulière des mods à ce jour.

## TODO

### Mods

- [3D_armor](https://forum.minetest.net/viewtopic.php?f=11&t=4654)
- [more blocks](https://forum.minetest.net/viewtopic.php?id=509)
- [signs_lib](https://forum.minetest.net/viewtopic.php?f=11&t=13762)
- [more_ore](https://forum.minetest.net/viewtopic.php?id=549)
- [windmills](https://forum.minetest.net/viewtopic.php?id=7440)
- [mesecons](http://mesecons.net/)
- [technic](https://forum.minetest.net/viewtopic.php?f=11&t=2538)
- [Farming Redo](https://forum.minetest.net/viewtopic.php?f=11&t=9019)
- [currency and economy](https://forum.minetest.net/viewtopic.php?f=11&t=21339)
- [sprint](https://github.com/GunshipPenguin/sprint)
- [Painting ?](https://forum.minetest.net/viewtopic.php?id=2588)
